package com.jb.accounts.constants;

public class AccountsConstants {
    private AccountsConstants() {
        // restrict instantiation
    }

    public static final String  SAVINGS = "Savings";
    public static final String  ADDRESS = "Thanh xuan, Ha Noi";
    public static final String  STATUS_201 = "201";
    public static final String  MESSAGE_201 = "Tạo Account thành công";
    public static final String  STATUS_200 = "200";
    public static final String  MESSAGE_200 = "Request processed successfully";
    public static final String  STATUS_417 = "417";
    public static final String  MESSAGE_417_UPDATE= "Update operation failed";
    public static final String  MESSAGE_417_DELETE= "Delete operation failed";
     public static final String  STATUS_500 = "500";
     public static final String  MESSAGE_500 = "An error occurred. Please try again";
}
